package ru.clevertec.box.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;
import ru.clevertec.box.dto.BoxDto;
import ru.clevertec.box.service.BoxProducerService;

@Service
@Slf4j
@RequiredArgsConstructor
public class BoxProducerServiceImpl implements BoxProducerService {

    private static final String CHANNEL_NAME = "sendBox-out-0";

    private final StreamBridge streamBridge;

    @Override
    public void sendBox(BoxDto boxDto) {
        log.info("send message: {}", boxDto);
        streamBridge.send(CHANNEL_NAME, boxDto);
    }

    @Override
    public void consumeBox(BoxDto boxDto) {
        log.info("Final {} has been received", boxDto);
    }
}
