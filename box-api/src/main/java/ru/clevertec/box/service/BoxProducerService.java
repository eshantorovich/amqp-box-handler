package ru.clevertec.box.service;

import ru.clevertec.box.dto.BoxDto;

public interface BoxProducerService {

    void sendBox(BoxDto boxDto);

    void consumeBox(BoxDto boxDto);
}
