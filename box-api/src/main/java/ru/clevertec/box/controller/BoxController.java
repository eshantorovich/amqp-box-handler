package ru.clevertec.box.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.clevertec.box.dto.BoxDto;
import ru.clevertec.box.service.BoxProducerService;

@RestController
@RequestMapping("/v1/boxes")
@RequiredArgsConstructor
public class BoxController {

    private final BoxProducerService boxProducerService;

    @PostMapping
    public void sendBox(@RequestBody BoxDto boxDto) {
        boxProducerService.sendBox(boxDto);
    }
}
