package ru.clevertec.box.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.clevertec.box.dto.BoxDto;
import ru.clevertec.box.service.BoxProducerService;

import java.util.function.Consumer;

@Configuration
@Slf4j
public class AppConfig {

    @Bean
    public Consumer<BoxDto> consumeBox(BoxProducerService boxProducerService) {
        return boxProducerService::consumeBox;
    }
}
