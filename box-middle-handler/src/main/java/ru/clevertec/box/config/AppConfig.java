package ru.clevertec.box.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.clevertec.box.dto.BoxDto;
import ru.clevertec.box.service.MiddleHandleService;

import java.util.function.Function;

@Configuration
public class AppConfig {

    @Bean
    public Function<BoxDto, BoxDto> forwardProcessor(MiddleHandleService middleService) {
        return middleService::handleBoxForwardDirection;
    }

    @Bean
    public Function<BoxDto, BoxDto> backwardProcessor(MiddleHandleService middleService) {
        return middleService::handleBoxBackwardDirection;
    }
}
