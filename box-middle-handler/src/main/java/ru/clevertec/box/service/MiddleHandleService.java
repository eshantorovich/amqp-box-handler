package ru.clevertec.box.service;

import ru.clevertec.box.dto.BoxDto;

public interface MiddleHandleService {

    BoxDto handleBoxForwardDirection(BoxDto boxDto);

    BoxDto handleBoxBackwardDirection(BoxDto boxDto);
}
