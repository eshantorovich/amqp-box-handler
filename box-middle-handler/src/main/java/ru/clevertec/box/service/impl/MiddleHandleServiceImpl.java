package ru.clevertec.box.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.clevertec.box.dto.BoxDto;
import ru.clevertec.box.service.MiddleHandleService;

import java.util.Random;

@Service
@Slf4j
@RequiredArgsConstructor
public class MiddleHandleServiceImpl implements MiddleHandleService {

    private final Random random = new Random();

    @Override
    public BoxDto handleBoxForwardDirection(BoxDto boxDto) {
        log.info("{} has been received", boxDto);
        boxDto.setFirstMiddleNumber(random.nextInt());
        log.info("return message: {}", boxDto);
        return boxDto;
    }

    @Override
    public BoxDto handleBoxBackwardDirection(BoxDto boxDto) {
        log.info("{} has been received", boxDto);
        boxDto.setSecondMiddleNumber(random.nextInt());
        log.info("return message: {}", boxDto);
        return boxDto;
    }
}
