package ru.clevertec.box.service;

import ru.clevertec.box.dto.BoxDto;

public interface BoxService {

    void handleBox(BoxDto boxDto);
}
