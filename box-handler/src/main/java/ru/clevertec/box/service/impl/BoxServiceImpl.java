package ru.clevertec.box.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;
import ru.clevertec.box.dto.BoxDto;
import ru.clevertec.box.service.BoxService;

import java.util.Random;

@Service
@Slf4j
@RequiredArgsConstructor
public class BoxServiceImpl implements BoxService {

    private static final String OUT_CHANNEL_NAME = "sendBox-out-0";

    private final StreamBridge streamBridge;
    private final Random random = new Random();

    @Override
    public void handleBox(BoxDto boxDto) {
        log.info("{} has been received", boxDto);
        boxDto.setHandlerNumber(random.nextInt());
        log.info("send message: {}", boxDto);
        streamBridge.send(OUT_CHANNEL_NAME, boxDto);
    }
}
