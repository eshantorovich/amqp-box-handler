package ru.clevertec.box.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BoxDto {

    String name;
    Integer firstMiddleNumber;
    Integer handlerNumber;
    Integer secondMiddleNumber;
}
